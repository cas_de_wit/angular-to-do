import { Component } from '@angular/core';
import { Todo } from 'src/app/models/Todo';

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.scss']
})
export class TodosComponent {
    todos: Todo[];

    inputTodo: string = ""

    constructor() { }

    ngOnInit(): void {
        this.todos = [
            {
                content: 'First todo',
                completed: false,
            },
            {
                content: 'Second todo',
                completed: true,
            }
        ]
    }

    toggleDone(id: number): void {
        this.todos.map((todo, index) => {
            if (index === id) {
                todo.completed = !todo.completed;
            }

            return todo;
        })
    }

    deleteTodo(id: number): void {
        this.todos = this.todos.filter((todo, index) => index !== id && todo);
    }

    addTodo() {
        this.todos.push({
            content: this.inputTodo,
            completed: false,
        });

        this.inputTodo = "";
    }
}
